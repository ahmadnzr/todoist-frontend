import axios from "axios";
import { BACKEND_URL } from "../config/api";
import { ACCESS_TOKEN } from "../config/localStorage";

export function getAccessToken() {
  try {
    return `Bearer ${JSON.parse(localStorage.getItem(ACCESS_TOKEN) || "null")}`;
  } catch {
    return null;
  }
}

export default axios.create({
  baseURL: BACKEND_URL,
});
