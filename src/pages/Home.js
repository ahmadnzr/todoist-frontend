import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import MainLayout from "../layouts/main";
import AuthContext from "../context/AuthContext";
import { logout } from "../services/authService";
import { getTasks } from "../services/taskService";
import Task from "../components/Task";
import "./Home.css";

function Home() {
  const [isLoading, setLoading] = useState(false);
  const [tasks, setTasks] = useState([]);
  const user = useContext(AuthContext);
  const navigate = useNavigate();

  // On mount and user changed
  useEffect(() => {
    if (!user) return;

    setLoading(true);

    // Get task from backend
    getTasks()
      .then(({ data }) => {
        setTasks(data.data?.tasks);
      })
      .catch(console.log)
      .finally(() => setLoading(false));
  }, [user]);

  // On user changed
  useEffect(() => {
    if (!user)
      navigate("/login", {
        replace: true,
      });
  }, [user, navigate]);

  function onLogoutClick() {
    logout()
      .then(() => navigate("/login", { replace: true }))
      .catch(console.log);
  }

  return (
    <MainLayout>
      <div className="Home-container">
        <h1>Todoist</h1>
        <p>Hello, {user?.email.split("@")[0]}!</p>
        <button className="Home-logoutButton" onClick={onLogoutClick}>
          Logout
        </button>

        {tasks.map((task) => (
          <Task {...task} />
        ))}
      </div>
    </MainLayout>
  );
}

export default Home;
