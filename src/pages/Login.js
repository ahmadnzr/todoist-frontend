import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useLocalStorage from "../hooks/useLocalStorage";
import MainLayout from "../layouts/main";
import LoginForm from "../components/LoginForm";
import AuthContext from "../context/AuthContext";
import { login } from "../services/authService";
import { ACCESS_TOKEN } from "../config/localStorage";
import "./Login.css";

function Login() {
  const [accessToken, setAccessToken] = useLocalStorage(ACCESS_TOKEN, null);
  const navigate = useNavigate();
  const user = useContext(AuthContext);

  useEffect(() => {
    if (!!user)
      navigate("/", {
        replace: true,
      });
  }, [user, navigate]);

  function onSubmit({ email, password }) {
    login(email, password)
      .then((credential) => {
        setAccessToken(credential?.user?.accessToken);
        navigate("/", { replace: true });
      })
      .catch(console.log);
  }

  useEffect(() => {}, [accessToken]);

  return (
    <MainLayout className="Login-container">
      <h1>Login</h1>
      <p>Welcome back Todoist!</p>

      <div>
        <LoginForm onSubmit={onSubmit} />
      </div>
    </MainLayout>
  );
}

export default Login;
