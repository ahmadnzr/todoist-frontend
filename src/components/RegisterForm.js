import { useState } from "react";
import "./RegisterForm.css";

function RegisterForm({ onSubmit }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function onRegistrationSubmit(e) {
    e.preventDefault();

    return onSubmit({ email, password });
  }

  return (
    <form onSubmit={onRegistrationSubmit} className="RegisterForm-form">
      <div>
        <label htmlFor="email">Email</label>
        <br />
        <input
          type="email"
          name="email"
          onChange={(e) => setEmail(e.target.value)}
          value={email}
        />
      </div>

      <div>
        <label htmlFor="password">Password</label>
        <br />
        <input
          type="password"
          name="password"
          onChange={(e) => setPassword(e.target.value)}
          value={password}
        />
      </div>

      <input type="submit" value="Register" />
    </form>
  );
}

export default RegisterForm;
