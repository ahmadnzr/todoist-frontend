function Task({ id, content, isCompleted, completedAt, dueAt }) {
  return (
    <div className="Task-container">
      <p>{content}</p>
    </div>
  );
}

export default Task;
