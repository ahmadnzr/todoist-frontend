// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import firebaseConfig from "../config/firebase";

export default initializeApp(firebaseConfig);
