import todoistService, { getAccessToken } from "./todoistService";

export function getTasks({
  content = undefined,
  isCompleted = undefined,
  dueAt = undefined,
} = {}) {
  return todoistService.get("/api/v1/tasks", {
    headers: {
      Authorization: getAccessToken(),
    },
    params: {
      content,
      isCompleted,
      dueAt,
    },
  });
}

export function postTasks({ content, dueAt }) {
  const body = {
    content,
    dueAt,
  };

  return todoistService.post("/api/v1/tasks", body, {
    headers: {
      Authorization: getAccessToken(),
    },
  });
}

export function putTask(id, { content, dueAt, isCompleted }) {
  const body = {
    content,
    isCompleted,
    dueAt,
  };

  return todoistService.put("/api/v1/tasks/" + id, body, {
    headers: {
      Authorization: getAccessToken(),
    },
  });
}

export function getTask(id) {
  return todoistService.get("/api/v1/tasks/" + id, {
    headers: {
      Authorization: getAccessToken(),
    },
  });
}

export function deleteTask(id) {
  return todoistService.delete("/api/v1/tasks/" + id, {
    headers: {
      Authorization: getAccessToken(),
    },
  });
}
